<div class="templateInfromacions form">
<?php echo $this->Form->create('TemplateInfromacion'); ?>
	<fieldset>
		<legend><?php echo __('Edit Template Infromacion'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('pipeline_id');
		echo $this->Form->input('content');
		echo $this->Form->input('type');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TemplateInfromacion.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('TemplateInfromacion.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Template Infromacions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Pipelines'), array('controller' => 'pipelines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pipeline'), array('controller' => 'pipelines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flow Passes'), array('controller' => 'flow_passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flow Pass'), array('controller' => 'flow_passes', 'action' => 'add')); ?> </li>
	</ul>
</div>
