<!-- Content -->
<div class="row">
    <div class="col-lg-12">
        <div class="tabs-wrapper tabs-no-header">
            <!-- Custom Field tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#"><?php echo __('Templates'); ?></a></li>                           
                <li><a  href="<?php echo $this->Html->url(array("controller" => "ContactCatalogs", "action" => "index")); ?>"><?php echo __('Contact'); ?></a></li>
            </ul>
                <div class="tab-content">
                    <!-- Deal Custom Field Tab -->
                    <div id="tab-product" class="tab-pane fade active in">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="clearfix">
                                    <div class="col-lg-6 col-sm-12 col-xs-12">
                                        <h1 class="pull-left"><?php echo __('Templates'); ?></h1>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 col-xs-6">
                                        <?php echo $this->Form->input('product_id', array('type' => 'text', 'class' => 'form-control search-data module-search', 'placeholder' => __('Quick Search Template'), 'data-name' => 'products', 'label' => false, 'div' => false)); ?>
                                    </div>
                                    <div class="col-lg-2 col-sm-6 col-xs-6">
                                        <div class="pull-right top-page-ui">
                                            <?php if ($this->Common->isStaffPermission('32')): ?>                    
                                                <a class="btn btn-primary pull-right" href="#" data-toggle="modal" data-target="#templateM">
                                                    <i class="fa fa-plus-circle fa-lg"></i> <?php echo __('Add Template'); ?>
                                                </a>                    
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-box no-header clearfix">					  
                                    <div class="main-box-body clearfix">
                                        <!-- Product List -->
                                        <div class="table-responsive">
                                            <div class="table-scrollable">
                                                <table class="table table-hover dataTable table-striped dataTables">
                                                    <thead>
                                                        <tr>
                                                            <th><?php echo __('Name'); ?></th>
                                                            <th><?php echo __('Type'); ?></th>
                                                            <th class="text-center"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if (!empty($templateInfromacions)) :

                                                            foreach ($templateInfromacions as $row) :

                                                                ?>
                                                                <tr  id="<?php echo 'row' . h($row['TemplateInfromacion']['id']); ?>">
                                                                    <td>
                                                                        <?php if ($this->Common->isStaffPermission('33')) : ?>
                                                                            <a href="javascript:void(0)"  data-type="text" data-pk="<?php echo h($row['TemplateInfromacion']['id']); ?>" data-name="name" data-url="TemplateInfromacions/edit"   class="editable editable-click vEdit" ref="popover" data-content="Edit Name" ><?php echo h($row['TemplateInfromacion']['name']); ?></a>
                                                                            <?php
                                                                        else :
                                                                            echo h($row['TemplateInfromacion']['name']);
                                                                        endif;

                                                                        ?>
                                                                    </td>
                                                                    <td>
																		<?= ($row['TemplateInfromacion']['type'] == 1) ? __('Email') : __('WhatsApp'); ?>
                                                                    </td>
                                                                    <td class="text-center">	
                                                                        <a class="table-link" ref="popover" data-content="View Template" href="<?php echo $this->Html->url(array("controller" => "TemplateInfromacions", "action" => "view", h($row['TemplateInfromacion']['id']))); ?>" ref="popover" data-content="View Product" >
                                                                            <i class="fa fa-eye"></i>
                                                                        </a>
                                                                        <?php if ($this->Common->isStaffPermission('34')): ?>
                                                                            <a class="table-link danger" href="#" ref="popover" data-content="Delete Template" data-toggle="modal" data-target="#delTemplateM" onclick="fieldU('TemplateInfromacionId',<?php echo h($row['TemplateInfromacion']['id']); ?>)" ref="popover" data-content="Delete Product" >
                                                                                <i class="fa fa-trash-o"></i>
                                                                            </a>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            endforeach;
                                                        endif;

                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>	
                                        </div>
                                        <!--End Product List -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                             
                    <!--End Deal Custom Field Tab -->
                </div>
        </div>						
    </div>
</div>
<!--/contact-->
<!--Add fields Modal--->
<div class="modal fade" id="templateM">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Extra Field'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('TemplateInfromacions', array('url' => array('controller' => 'TemplateInfromacions', 'action' => 'add'), 'inputDefaults' => array('label' => false, 'div' => false), 'class' => 'vForm')); ?>
                <div class="form-group">
                    <label><?php echo __('Template Name'); ?></label>
                    <?php echo $this->Form->input('TemplateInfromacion.name', array('type' => 'text', 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Template Name'))); ?>	
                </div>
                <div class="form-group">
                    <label><?php echo __('Pipeline'); ?></label>
                    <?php echo $this->Form->input('TemplateInfromacion.pipeline_id', array('type' => 'select', 'class' => 'select-box-search full-width', 'options' => array($this->Common->getPipelineList()), 'empty' => __('Select Pipeline'))); ?>	
                </div>
				<div class="form-group">
                    <label><?php echo __('Type'); ?></label>
                    <?php echo $this->Form->input('TemplateInfromacion.type', array('type' => 'select', 'options' => array('1' => __('Email'), '2' => __('WhatsApp')), 'empty' => __('Select Type'), 'class' => 'select-box-search full-width')); ?>	
                </div>
				<div class="form-group">
				    <?php echo $this->Form->input('TemplateInfromacion.content', array('value' => '', 'type' => 'hidden', 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Template content'))); ?>	
                </div>
            </div>
            <div class="modal-footer">			
                <?php echo $this->Form->Submit(__('Save'), array('class' => 'btn btn-primary blue', 'id' => 'addTemplateSubmitBtn', 'div' => false)); ?>
                <button class="btn default" data-dismiss="modal" type="button"><?php echo __('Close'); ?></button>
            </div>
            <?php echo $this->Form->end(); ?>	
            <?php echo $this->Js->writeBuffer(); ?>
        </div>
    </div>
</div>
<!--/Add Fields Modal-->
<!-- Delete Extra Field modal -->
<div class="modal fade" id="delTemplateM">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title"><?php echo __('Confirmation'); ?></h4>
            </div>
            <?php echo $this->Form->create('TemplateInfromacion', array('url' => array('action' => 'delete'))); ?>
            <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
            <div class="modal-body">						
                <p><?php echo __('Are you sure to delete this Extra Field ?'); ?>  </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary delSubmit"  type="button"><?php echo __('Yes'); ?></button>
                <button class="btn default" data-dismiss="modal" type="button"><?php echo __('No'); ?></button>
            </div>
            <?php echo $this->Form->end(); ?>
            <?php echo $this->Js->writeBuffer(); ?>
        </div>
    </div>
</div>
<!-- /Delete modal -->