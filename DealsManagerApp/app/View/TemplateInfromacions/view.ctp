<?php
/**
 * View for product details page.
 * 
 * @author:   AnkkSoft.com
 * @Copyright: AnkkSoft 2019
 * @Website:   https://www.ankksoft.com
 * @CodeCanyon User:  https://codecanyon.net/user/codeloop 
 */

?>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="main-box no-header clearfix">	
                <div class="main-box-body clearfix">
                    <div class="row" >  
                        <div class="col-sm-12 contact-view-box text-center">
                            <h1><?= h($templateInfromacion['TemplateInfromacion']['name']); ?></h1>                           
                        </div>
                        <table class="table">
                            <tbody><tr>
                                    <td><strong><?php echo __('Name'); ?></strong></td>
                                    <td><?= h($templateInfromacion['TemplateInfromacion']['name']); ?></td>
                                </tr>
                                <tr>
									<td><strong><?php echo __('Pipeline'); ?></strong></td>
                                    <td><?= h($templateInfromacion['Pipeline']['name']); ?></td>
								</tr> 
								<tr>
                                    <td><strong><?php echo __('Type'); ?></strong></td>
									<td><?= ($templateInfromacion['TemplateInfromacion']['type'] == 1) ? __('Email') : __('WhatsApp'); ?></td>
                                </tr>       
                            </tbody>
                        </table>
                    </div>                                           
                    <div class="row">
                        <div class="contact-box-heading">
                            <span><strong><?php echo __('Edit Content'); ?></strong></span>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
							<div class="modal-body">
                            <?php echo $this->Form->create('TemplateInfromacions', array('url' => array('controller' => 'TemplateInfromacions', 'action' => 'updateContent'), 'inputDefaults' => array('label' => false, 'div' => false), 'class' => 'vForm')); ?>
                            <?php echo $this->Form->input('TemplateInfromacion.id', array('value' => $templateInfromacion['TemplateInfromacion']['id'], 'type' => 'hidden', 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Template content'))); ?>	
                                <?php if ($templateInfromacion['TemplateInfromacion']['type']==1) : ?>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('TemplateInfromacion.content', array('type' => 'textarea','id'=>'editor1', 'row' => 10,'cols'=>20,'value' => $templateInfromacion['TemplateInfromacion']['content'], 'class' => 'form-control input-inline input-medium')); ?>
                                        <script type="text/javascript" > CKEDITOR.replace('editor1');</script>
                                        </div>
                                    <?php
                                else :
                                    ?>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('TemplateInfromacion.content', array('type' => 'textarea','data-emojiable'=>'true', 'row' => 10,'cols'=>20,'value' => $templateInfromacion['TemplateInfromacion']['content'], 'class' => 'form-control input-inline input-medium')); ?>
                                        </div>
                                    <script>
                                        $(function() {
                                            // Initializes and creates emoji set from sprite sheet
                                            window.emojiPicker = new EmojiPicker({
                                            emojiable_selector: '[data-emojiable=true]',
                                            assetsPath: '/DealsManagerApp/img/emoji-poker',
                                            popupButtonClasses: 'fa fa-smile-o'
                                            });
                                            // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
                                            // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
                                            // It can be called as many times as necessary; previously converted input fields will not be converted again
                                            window.emojiPicker.discover();
                                        });
                                    </script>
                                <?php
                                    endif;
                                ?>
							</div>
							<div class="modal-footer">			
							<?php echo $this->Form->Submit(__('Save'), array('class' => 'btn btn-primary blue', 'id' => 'EditContentTemplateSubmitBtn', 'div' => false)); ?>
							</div>
							<?php echo $this->Form->end(); ?>	
							<?php echo $this->Js->writeBuffer(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>						
    </div>
</div>