<div class="flows form">
<?php echo $this->Form->create('Flow'); ?>
	<fieldset>
		<legend><?php echo __('Edit Flow'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('stage_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Flow.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Flow.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Flows'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stages'), array('controller' => 'stages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stage'), array('controller' => 'stages', 'action' => 'add')); ?> </li>
	</ul>
</div>
