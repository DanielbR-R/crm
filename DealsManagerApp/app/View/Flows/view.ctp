<!-- Add stage modal-->
<div class="modal fade" id="FlowPassM">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo __('Add Pass'); ?></h4>
            </div>
            <?php echo $this->Form->create('FlowPass', array('url' => array('controller' => 'FlowPasses', 'action' => 'add'), 'inputDefaults' => array('label' => false, 'div' => false), 'class' => 'vForm')); ?>
            <div class="modal-body">													
                <div class="form-group">
                    <label><?php echo __('Action Type');?></label>
                    <?php echo $this->Form->input('action_type', array('type' => 'select', 'options' => array(1 => __('Email'), 2 => __('WhatsApp'), 3 => __('Timelapse')), 'empty' => __('Select Action'), 'class' => 'select-box-search full-width')); ?>	
                </div>
                <div style="display:none;" id="contact">
                    <div class="form-group">
                        <label><?php echo __('Template'); ?></label>
                        <?php echo $this->Form->input('template', array('type' => 'select','class' => 'select-box-search full-width', 'label' => false,'empty' => __('Select Template'))); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Contact'); ?></label>
                        <?php echo $this->Form->input('contact', array('type' => 'select', 'class' => 'select-box-search full-width', 'label' => false,'empty' => __('Select Contact'))); ?>
                    </div>
                </div>
                <div style="display:none;" id="timelapse">
                    <div class="form-group">
                        <label><?php echo __('Time'); ?></label>
                        <?php echo $this->Form->input('Timelapse', array('type' => 'select','options' => array(1 => __('Hour'), 2 => __('Day'), 3 => __('Week'), 4 => __('Month')), 'class' => 'select-box-search full-width', 'label' => false,'empty' => __('Select Timelapse'))); ?>
                    </div>
                    <div style="display:none;" id="hour">
                        <div class="form-group">
                        <?php echo $this->Form->input('timeHour', array('type' => 'number','min'=>'1', 'max'=>'24', 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Timelapse'))); ?>	
                        </div>
                    </div>
                    <div style="display:none;" id="Day">
                        <div class="form-group">
                            <?php echo $this->Form->input('timeDay', array('type' => 'number','min'=>'1', 'max'=>'7', 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Timelapse'))); ?>	
                        </div>
                    </div>
                    <div style="display:none;" id="Week">
                        <div class="form-group">
                            <?php echo $this->Form->input('timeWeek', array('type' => 'number','min'=>'1', 'max'=>'152', 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Timelapse'))); ?>	
                        </div>
                    </div>
                    <div style="display:none;" id="Month">
                        <div class="form-group">
                            <?php echo $this->Form->input('timeMonth', array('type' => 'number','min'=>'1', 'max'=>'12', 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Timelapse'))); ?>	
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->input('flow_id', array('type' => 'hidden','value'=>$id_flow, 'class' => 'form-control input-inline input-medium', 'label' => false)); ?>	
                <?php echo $this->Form->input('position', array('type' => 'hidden','value'=>1, 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Stage Name'), 'label' => false)); ?>
            </div>
            <div class="modal-footer">			
                <button class="btn btn-primary blue btn-sm" type="submit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
                <button class="btn default btn-sm" data-dismiss="modal" type="button"><i class="fa fa-times"></i> <?php echo __('Close'); ?></button>
            </div>
            <?php echo $this->Form->end(); ?>	
            <?php echo $this->Js->writeBuffer(); ?>
        </div>
    </div>
</div>
<!-- Delete stage modal-->
<div class="modal fade" id="delFlowPassM">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title"><?php echo __('Confirmation'); ?></h4>
            </div>
            <?php echo $this->Form->create('Flowpass', array('url' => array('action' => 'delete'))); ?>
            <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
            <div class="modal-body">						
                <p><?php echo __('Are you sure to delete this Flow Pass ?'); ?> </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary delSubmitS"  type="button"><?php echo __('Yes'); ?></button>
                <button class="btn default" data-dismiss="modal" type="button"><?php echo __('No'); ?></button>
            </div>
            <?php echo $this->Form->end(); ?>
            <?php echo $this->Js->writeBuffer(); ?>
        </div>
    </div>
</div>
<!-- /Delete modal -->
<!-- Content -->

<div class="row"> 
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="clearfix">
                    <h1 class="pull-left"><?php echo __('Flow Pass'); ?></h1>
                    <div class="pull-right top-page-ui">
                        <a class="btn btn-primary pull-right" href="#" data-toggle="modal" data-target="#FlowPassM">
                            <i class="fa fa-plus-circle fa-lg"></i> <?php echo __('Add Flow Pass'); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="main-box clearfix">	
                    <div class="tabs-wrapper tabs-no-header">
                        <div class="tab-content">
                        <div id="tab-home" class="tab-pane fade active in">
                            <div class="row manager-index">
                                <h1><?php echo __($FlowPass[0]['Flow']['name']); ?></h1>  
                            </div>
                        </div>
                            <!-- Pipeline wise stages -->
                                <div class="row">
                                    <ul id="sortable" class="sortabless2">
                                    <?php $count = 1; ?>
                                        <?php foreach ($FlowPass as $pass): 
                                            ?>
                                            <li class="ui-state-default dd-item dd-item-list stage-li"  id="item-<?= h($pass['FlowPass']['id']); ?>">
                                                <div class="dd-handle">
                                                    <?php 
                                                        switch ($pass['FlowPass']['action_type']) {
                                                            case 1:
                                                    ?>
                                                            <a data-type="readonly"><?php echo __('Template: '.$pass['TemplateInfromacion']['name']); echo __(' / Action: Email');?></a><br>
                                                    <?php
                                                            break;
                                                            case 2:
                                                    ?>
                                                            <a data-type="readonly"><?php echo __('Template: '.$pass['TemplateInfromacion']['name']); echo __(' / Action: WhatsApp');?></a><br>
                                                    <?php
                                                            break;
                                                            case 3:
                                                    ?>
                                                            <a data-type="readonly"><?php echo __('Time: '.$pass['FlowPass']['time']); echo __(' / Action: Timelapse');?></a><br>
                                                    <?php
                                                            break;
                                                        }
                                                    ?>
                                                    <div class="nested-links">
                                                        <a class="nested-link" href="#"  data-toggle="modal" data-target="#delFlowPassM" onclick="fieldU('FlowpassId',<?= h($pass['FlowPass']['id']); ?>)"><i class="fa fa-trash-o fa-red"></i></a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php
                                            $count++;
                                            endforeach;
                                        ?>
                                    </ul>
                                </div>
                            <!--End Pipeline wise stages -->
                        </div>
                    </div>
                </div>
            </div>
        </div>						
    </div>
</div>
<!-- /Content -->