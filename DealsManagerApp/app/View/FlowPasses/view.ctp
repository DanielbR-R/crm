<div class="flowPasses view">
<h2><?php echo __('Flow Pass'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($flowPass['flowPass']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Template Infromacion'); ?></dt>
		<dd>
			<?php echo $this->Html->link($flowPass['TemplateInfromacion']['name'], array('controller' => 'template_infromacions', 'action' => 'view', $flowPass['TemplateInfromacion']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Flow'); ?></dt>
		<dd>
			<?php echo $this->Html->link($flowPass['Flow']['name'], array('controller' => 'flows', 'action' => 'view', $flowPass['Flow']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contact Catalog'); ?></dt>
		<dd>
			<?php echo $this->Html->link($flowPass['ContactCatalog']['value'], array('controller' => 'contact_catalogs', 'action' => 'view', $flowPass['ContactCatalog']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action Type'); ?></dt>
		<dd>
			<?php echo h($flowPass['FlowPass']['action_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Position'); ?></dt>
		<dd>
			<?php echo h($flowPass['FlowPass']['position']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Flow Pass'), array('action' => 'edit', $flowPass['FlowPass']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Flow Pass'), array('action' => 'delete', $flowPass['FlowPass']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $flowPass['FlowPass']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Flow Passes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flow Pass'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Template Infromacions'), array('controller' => 'template_infromacions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Template Infromacion'), array('controller' => 'template_infromacions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flows'), array('controller' => 'flows', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flow'), array('controller' => 'flows', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contact Catalogs'), array('controller' => 'contact_catalogs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact Catalog'), array('controller' => 'contact_catalogs', 'action' => 'add')); ?> </li>
	</ul>
</div>

<!-- Content -->
<div class="row"> 
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="clearfix">
                    <h1 class="pull-left"><?php echo __('Flow Pass'); ?></h1>
                    <div class="pull-right top-page-ui">
                        <a class="btn btn-primary pull-right" href="#" data-toggle="modal" data-target="#stageM">
                            <i class="fa fa-plus-circle fa-lg"></i> <?php echo __('Add Flow Pass'); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="main-box clearfix">	
                    <div class="tabs-wrapper tabs-no-header">
                        <div class="tab-content">
                            <!-- General tab -->
                            <div id="tab-home" class="tab-pane fade active in">
                                <div class="row manager-index">
                                    <?php echo $this->Html->image('stages.png'); ?>
                                    <h1><?php echo __('Select Pipeline for Stages'); ?></h1>  
                                </div>
                            </div>
                            <!--End General tab -->                           
                            <?php foreach ($pipelines as $row): ?>
                                <!-- Pipeline wise stages -->
                                <div id="tab-home<?= h($row['Pipeline']['id']); ?>" class="tab-pane fade">
                                    <div class="row">
                                        <ul id="sortable" class="sortabless">
                                            <?php $count = 1; ?>
                                            <?php foreach ($row['Stage'] as $stage): ?>
                                                <li class="ui-state-default dd-item dd-item-list stage-li"  id="item-<?= h($stage['id']); ?>">
                                                    <div class="dd-handle">
                                                        <a data-type="text" data-pk="<?= h($stage['id']); ?>" data-url="stages/edit"  class="editable editable-click vEdit"  ref="popover" data-content="Edit Stage Name"><?= h($stage['name']); ?></a>
                                                        <div class="nested-links">
                                                            <a class="nested-link" href="#"  data-toggle="modal" data-target="#delStageM" onclick="fieldU('StageId',<?= h($stage['id']); ?>)"><i class="fa fa-trash-o fa-red"></i></a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php
                                                $count++;
                                            endforeach;
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <!--End Pipeline wise stages -->
                                <?php
                                $count++;
                            endforeach;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>						
    </div>
</div>
<!-- /Content -->