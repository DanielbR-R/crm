<div class="flowPasses index">
	<h2><?php echo __('Flow Passes'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('template_infromacion_id'); ?></th>
			<th><?php echo $this->Paginator->sort('flow_id'); ?></th>
			<th><?php echo $this->Paginator->sort('contact_catalog_id'); ?></th>
			<th><?php echo $this->Paginator->sort('action_type'); ?></th>
			<th><?php echo $this->Paginator->sort('position'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($flowPasses as $flowPass): ?>
	<tr>
		<td><?php echo h($flowPass['FlowPass']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($flowPass['TemplateInfromacion']['name'], array('controller' => 'template_infromacions', 'action' => 'view', $flowPass['TemplateInfromacion']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($flowPass['Flow']['name'], array('controller' => 'flows', 'action' => 'view', $flowPass['Flow']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($flowPass['ContactCatalog']['value'], array('controller' => 'contact_catalogs', 'action' => 'view', $flowPass['ContactCatalog']['id'])); ?>
		</td>
		<td><?php echo h($flowPass['FlowPass']['action_type']); ?>&nbsp;</td>
		<td><?php echo h($flowPass['FlowPass']['position']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $flowPass['FlowPass']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $flowPass['FlowPass']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $flowPass['FlowPass']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $flowPass['FlowPass']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Flow Pass'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Template Infromacions'), array('controller' => 'template_infromacions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Template Infromacion'), array('controller' => 'template_infromacions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flows'), array('controller' => 'flows', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flow'), array('controller' => 'flows', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contact Catalogs'), array('controller' => 'contact_catalogs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact Catalog'), array('controller' => 'contact_catalogs', 'action' => 'add')); ?> </li>
	</ul>
</div>
