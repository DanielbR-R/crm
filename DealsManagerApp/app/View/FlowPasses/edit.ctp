<div class="flowPasses form">
<?php echo $this->Form->create('FlowPass'); ?>
	<fieldset>
		<legend><?php echo __('Edit Flow Pass'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('template_infromacion_id');
		echo $this->Form->input('flow_id');
		echo $this->Form->input('contact_catalog_id');
		echo $this->Form->input('action_type');
		echo $this->Form->input('position');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('FlowPass.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('FlowPass.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Flow Passes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Template Infromacions'), array('controller' => 'template_infromacions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Template Infromacion'), array('controller' => 'template_infromacions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flows'), array('controller' => 'flows', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flow'), array('controller' => 'flows', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contact Catalogs'), array('controller' => 'contact_catalogs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact Catalog'), array('controller' => 'contact_catalogs', 'action' => 'add')); ?> </li>
	</ul>
</div>
