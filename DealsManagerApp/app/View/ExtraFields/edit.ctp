<div class="extraFields form">
<?php echo $this->Form->create('ExtraField'); ?>
	<fieldset>
		<legend><?php echo __('Edit Extra Field'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('pipeline_id');
		echo $this->Form->input('field_name');
		echo $this->Form->input('type');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ExtraField.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ExtraField.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Extra Fields'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Pipelines'), array('controller' => 'pipelines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pipeline'), array('controller' => 'pipelines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Deals'), array('controller' => 'product_deals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Deal'), array('controller' => 'product_deals', 'action' => 'add')); ?> </li>
	</ul>
</div>
