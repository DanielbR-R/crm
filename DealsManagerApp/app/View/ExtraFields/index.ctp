<!-- Content -->
<div class="row">
    <div class="col-lg-12">
        <div class="tabs-wrapper tabs-no-header">
            <!-- Custom Field tabs -->
            <ul class="nav nav-tabs">
                <li ><a href="<?php echo $this->Html->url(array("controller" => "Products", "action" => "index")); ?>"><?php echo __('Product'); ?></a></li>                           
                <li class="active"><a data-toggle="tab"  href="#"><?php echo __('Custom Fields'); ?></a></li>
            </ul>
                <div class="tab-content">
                    <!-- Contact Custom Field Tab -->
                    <div id="tab-field" class="tab-pane fade active in">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="clearfix">
                                    <div class="col-lg-6 col-sm-12 col-xs-12">
                                        <h1 class="pull-left"><?php echo __('Custom Fields'); ?></h1>
                                    </div>
                                    <div class="col-lg-2 col-sm-6 col-xs-6">
                                        <div class="pull-right top-page-ui">
                                            <?php if ($this->Common->isStaffPermission('32')): ?>
                                                <a class="btn btn-primary pull-right" href="#" data-toggle="modal" data-target="#fieldM">
                                                    <i class="fa fa-plus-circle fa-lg"></i> <?php echo __('Add Field'); ?>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-box no-header clearfix">					  
                                    <div class="main-box-body clearfix">
                                        <!-- Field List -->
                                        <div class="table-responsive">
                                            <div class="table-scrollable">
                                                <table class="table table-hover dataTable table-striped dataTables">
                                                    <thead>
                                                        <tr>
                                                            <th><?php echo __('Name'); ?></th>
                                                            <th><?php echo __('Type'); ?></th>
                                                            <th class="text-center"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        
                                                        if (!empty($extraFields)) :
                                                            foreach ($extraFields as $row) :
                                                                ?>
                                                                <tr  id="<?php echo 'row' . h($row['ExtraField']['id']); ?>">
                                                                    <td>
                                                                        <?php if ($this->Common->isStaffPermission('33')) : ?>
                                                                            <a href="javascript:void(0)"  data-type="text" data-pk="<?php echo h($row['ExtraField']['id']); ?>" data-name="name" data-url="extrafields/edit"  class="editable editable-click vEdit" ref="popover" data-content="Edit Name" ><?php echo h($row['ExtraField']['field_name']); ?></a>
                                                                            <?php
                                                                        else :
                                                                            echo h($row['ExtraField']['name']);
                                                                        endif;
                                                                        ?>
                                                                    </td>
                                                                    <td>
                                                                        <?= ($row['ExtraField']['type'] == 1) ? __('Text') : __('Text Area'); ?>
                                                                    </td>
                                                                    <td class="text-center">	
                                                                        <?php if ($this->Common->isStaffPermission('34')): ?>
                                                                            <a class="table-link danger" href="#" ref="popover" data-content="Delete Field" data-toggle="modal" data-target="#delExtraFieldM" onclick="fieldU('ExtraFieldId',<?php echo h($row['ExtraField']['id']); ?>)" ref="popover" data-content="Delete Field" >
                                                                                <i class="fa fa-trash-o"></i>
                                                                            </a>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            endforeach;
                                                        endif;

                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>	
                                        </div>
                                        <!--End Product List -->
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <!--End Contact Custom Field  Tab -->
                </div>
        </div>						
    </div>
</div>
<!--/contact-->
<!--Add fields Modal--->
<div class="modal fade" id="fieldM">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Extra Field'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('ExtraFields', array('url' => array('controller' => 'ExtraFields', 'action' => 'add'), 'inputDefaults' => array('label' => false, 'div' => false), 'class' => 'vForm')); ?>
                <div class="form-group">
                    <label><?php echo __('Field Name'); ?></label>
                    <?php echo $this->Form->input('ExtraField.field_name', array('type' => 'text', 'class' => 'form-control input-inline input-medium', 'Placeholder' => __('Field Name'))); ?>	
                </div>
                <div class="form-group">
                    <label><?php echo __('Type'); ?></label>
                    <?php echo $this->Form->input('ExtraField.type', array('type' => 'select', 'options' => array('1' => __('Text Field'), '2' => __('Text Area')), 'class' => 'select-box-search full-width')); ?>	
                </div>
                <div class="form-group">
                    <label><?php echo __('Pipeline'); ?></label>
                    <?php echo $this->Form->input('ExtraField.pipeline_id', array('type' => 'select', 'class' => 'select-box-search full-width', 'options' => array($this->Common->getPipelineList()), 'empty' => __('Select Pipeline'))); ?>	
                </div>
            </div>
            <div class="modal-footer">			
                <?php echo $this->Form->Submit(__('Save'), array('class' => 'btn btn-primary blue', 'id' => 'addExtraFieldSubmitBtn', 'div' => false)); ?>
                <button class="btn default" data-dismiss="modal" type="button"><?php echo __('Close'); ?></button>
            </div>
            <?php echo $this->Form->end(); ?>	
            <?php echo $this->Js->writeBuffer(); ?>
        </div>
    </div>
</div>
<!--/Add Fields Modal-->
<!-- Delete Extra Field modal -->
<div class="modal fade" id="delExtraFieldM">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title"><?php echo __('Confirmation'); ?></h4>
            </div>
            <?php echo $this->Form->create('ExtraField', array('url' => array('action' => 'delete'))); ?>
            <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
            <div class="modal-body">						
                <p><?php echo __('Are you sure to delete this Extra Field ?'); ?>  </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary delSubmit"  type="button"><?php echo __('Yes'); ?></button>
                <button class="btn default" data-dismiss="modal" type="button"><?php echo __('No'); ?></button>
            </div>
            <?php echo $this->Form->end(); ?>
            <?php echo $this->Js->writeBuffer(); ?>
        </div>
    </div>
</div>
<!-- /Delete modal -->