<div class="extraFields view">
<h2><?php echo __('Extra Field'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($extraField['ExtraField']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pipeline'); ?></dt>
		<dd>
			<?php echo $this->Html->link($extraField['Pipeline']['name'], array('controller' => 'pipelines', 'action' => 'view', $extraField['Pipeline']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field Name'); ?></dt>
		<dd>
			<?php echo h($extraField['ExtraField']['field_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($extraField['ExtraField']['type']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Extra Field'), array('action' => 'edit', $extraField['ExtraField']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Extra Field'), array('action' => 'delete', $extraField['ExtraField']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $extraField['ExtraField']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Extra Fields'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Extra Field'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pipelines'), array('controller' => 'pipelines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pipeline'), array('controller' => 'pipelines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Deals'), array('controller' => 'product_deals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Deal'), array('controller' => 'product_deals', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Product Deals'); ?></h3>
	<?php if (!empty($extraField['ProductDeal'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Deal Id'); ?></th>
		<th><?php echo __('Extra Field Id'); ?></th>
		<th><?php echo __('Value'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($extraField['ProductDeal'] as $productDeal): ?>
		<tr>
			<td><?php echo $productDeal['id']; ?></td>
			<td><?php echo $productDeal['product_id']; ?></td>
			<td><?php echo $productDeal['deal_id']; ?></td>
			<td><?php echo $productDeal['extra_field_id']; ?></td>
			<td><?php echo $productDeal['value']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'product_deals', 'action' => 'view', $productDeal['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'product_deals', 'action' => 'edit', $productDeal['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'product_deals', 'action' => 'delete', $productDeal['id']), array('confirm' => __('Are you sure you want to delete # %s?', $productDeal['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Product Deal'), array('controller' => 'product_deals', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
