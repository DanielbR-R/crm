<div class="contactCatalogs view">
<h2><?php echo __('Contact Catalog'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($contactCatalog['ContactCatalog']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($contactCatalog['ContactCatalog']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($contactCatalog['ContactCatalog']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($contactCatalog['ContactCatalog']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($contactCatalog['ContactCatalog']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contact Catalog'), array('action' => 'edit', $contactCatalog['ContactCatalog']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Contact Catalog'), array('action' => 'delete', $contactCatalog['ContactCatalog']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $contactCatalog['ContactCatalog']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Contact Catalogs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact Catalog'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flow Passes'), array('controller' => 'flow_passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flow Pass'), array('controller' => 'flow_passes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Flow Passes'); ?></h3>
	<?php if (!empty($contactCatalog['FlowPass'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Template Infromacion Id'); ?></th>
		<th><?php echo __('Flow Id'); ?></th>
		<th><?php echo __('Contact Catalog Id'); ?></th>
		<th><?php echo __('Action Type'); ?></th>
		<th><?php echo __('Position'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($contactCatalog['FlowPass'] as $flowPass): ?>
		<tr>
			<td><?php echo $flowPass['id']; ?></td>
			<td><?php echo $flowPass['template_infromacion_id']; ?></td>
			<td><?php echo $flowPass['flow_id']; ?></td>
			<td><?php echo $flowPass['contact_catalog_id']; ?></td>
			<td><?php echo $flowPass['action_type']; ?></td>
			<td><?php echo $flowPass['position']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'flow_passes', 'action' => 'view', $flowPass['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'flow_passes', 'action' => 'edit', $flowPass['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'flow_passes', 'action' => 'delete', $flowPass['id']), array('confirm' => __('Are you sure you want to delete # %s?', $flowPass['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Flow Pass'), array('controller' => 'flow_passes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
