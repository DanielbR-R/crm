<div class="contactCatalogs form">
<?php echo $this->Form->create('ContactCatalog'); ?>
	<fieldset>
		<legend><?php echo __('Add Contact Catalog'); ?></legend>
	<?php
		echo $this->Form->input('value');
		echo $this->Form->input('type');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Contact Catalogs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Flow Passes'), array('controller' => 'flow_passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flow Pass'), array('controller' => 'flow_passes', 'action' => 'add')); ?> </li>
	</ul>
</div>
