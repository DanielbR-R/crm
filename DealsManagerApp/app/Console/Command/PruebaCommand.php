<?php
namespace App\Shell;

use Cake\Console\Shell;

/**
 * User shell command.
 */
class UserShell extends Shell
{
	//public $uses = array('User');
    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out('Hello world.');
    }
	
	public function hey_there($name='no hay nombre') 
	{
        $this->out('Hey there ' . $name);
    }
	
}
