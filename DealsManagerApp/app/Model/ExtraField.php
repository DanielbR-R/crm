<?php
App::uses('AppModel', 'Model');
/**
 * ExtraField Model
 *
 * @property Pipeline $Pipeline
 * @property ProductDeal $ProductDeal
 */
class ExtraField extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'extra_field';

/**
     * This function is used to get all products
     *
     * @access public
     * @return array
     */
    public function getAllExtraFiels()
    {
        //query
        $result = $this->find('all', array('order' => array('extrafield.id DESC')));
        return $result;
    }


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Pipeline' => array(
			'className' => 'Pipeline',
			'foreignKey' => 'pipeline_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ProductDeal' => array(
			'className' => 'ProductDeal',
			'foreignKey' => 'extra_field_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
