<?php
App::uses('AppModel', 'Model');
/**
 * Flow Model
 *
 * @property Stage $Stage
 */
class Flow extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'flow';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Stage' => array(
			'className' => 'Stage',
			'foreignKey' => 'stage_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
