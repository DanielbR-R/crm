<?php
App::uses('AppModel', 'Model');
/**
 * FlowPass Model
 *
 * @property TemplateInfromacion $TemplateInfromacion
 * @property Flow $Flow
 * @property ContactCatalog $ContactCatalog
 */
class FlowPass extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'flow_pass';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'TemplateInfromacion' => array(
			'className' => 'TemplateInfromacion',
			'foreignKey' => 'template_infromacion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Flow' => array(
			'className' => 'Flow',
			'foreignKey' => 'flow_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ContactCatalog' => array(
			'className' => 'ContactCatalog',
			'foreignKey' => 'contact_catalog_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
