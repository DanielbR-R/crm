<?php
App::uses('AppModel', 'Model');
/**
 * ContactCatalog Model
 *
 * @property FlowPass $FlowPass
 */
class ContactCatalog extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'contact_catalog';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'FlowPass' => array(
			'className' => 'FlowPass',
			'foreignKey' => 'contact_catalog_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
