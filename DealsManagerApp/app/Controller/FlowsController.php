<?php
App::uses('AppController', 'Controller');
/**
 * Flows Controller
 *
 * @property Flow $Flow
 * @property PaginatorComponent $Paginator
 */
class FlowsController extends AppController {

/**
     * This controller uses following models
     *
     * @var array
     */
    public $uses = array('Flow','FlowPass','ContactCatalog', 'Pipeline');

/**
     * This controller uses following helpers
     *
     * @var array
     */
    var $helpers = array('Html', 'Form', 'Js', 'Paginator', 'Time');

    /**
     * This controller uses following components
     *
     * @var array
     */
    var $components = array('Auth', 'Cookie', 'Session', 'Paginator', 'RequestHandler', 'Flash');

	/**
     * Called before the controller action.  You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        //check if login
        $this->checkLogin();
        //set layout
        $this->layout = 'admin';
        //check if admin or staff
        $this->checkAdminStaff();
    }

	/**
 * index method
 *
 * @return void
 */
	public function index() {
        $this->Flow->recursive = 0;
        $FlowPass=$this->FlowPass->find('all');
        /* $pipelines = $this->Pipeline->getPipelines();
        var_dump($pipelines[0]['Stage'][0]);
        die(); */
		$this->set('flows', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 * this function shows the flow steps instead of the flow detail
 */
	public function view($id = null) {
        $id_flow=$id;
		if (!$this->Flow->exists($id)) {
            throw new NotFoundException(__('Invalid flow'));
            $id_flow=1;
		}
        $flowpassfind = array('conditions' => array('FlowPass.flow_id'=> $id),'order' => 'FlowPass.position');
        $FlowPass=$this->FlowPass->find('all', $flowpassfind);
        $templateInfromacions = $this->FlowPass->TemplateInfromacion->find('list');
        $flows = $this->FlowPass->Flow->find('list');
        $contactCatalogs = $this->FlowPass->ContactCatalog->find('list');
        $contactCatalogsValue = $this->FlowPass->ContactCatalog->find('list', array('fields' => array('id', 'value')));
		$this->set(compact('templateInfromacions', 'flows', 'contactCatalogs','FlowPass','contactCatalogsValue','id_flow'));
    }


/**
 * add method
 *
 * @return void
 */
	public function add() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('32');
		if ($this->request->is('post')) {
			$this->Flow->create();
			if ($this->Flow->save($this->request->data)) {
				//success message
                $this->Flash->success(__('Request has been completed.'), array('key' => 'success', 'params' => array('class' => 'alert alert-info')));
            } else {
                //failure message
                $this->Flash->success(__('Request has been not completed.'), array('key' => 'fail', 'params' => array('class' => 'alert alert-danger')));
			}
			return $this->redirect(
				array('controller' => 'Flows', 'action' => 'index')
			);
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('33');
        //--------- Post request  -----------
        if ($this->request->is('post')) {
            //--------- Ajax request  -----------
            if ($this->RequestHandler->isAjax()) {
				$this->layout = 'ajax';
				//common variables
                $this->request->data['Flow']['id'] = $this->request->data['pk'];  
                $this->request->data['Flow']['name'] = $this->request->data['value'];
				//save product
                $success = $this->Flow->save($this->request->data);
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success');
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);
                }
            }
        }
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('34');
		$FlowId = $this->request->data['Flow']['id'];
        //if product id exist
        if (!empty($FlowId)) {
            //--------- Post/Ajax request  -----------
            if ($this->request->isPost() || $this->RequestHandler->isAjax()) {
                //delete product
				$success = $this->Flow->delete($FlowId, false);
				//$success = true;
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success', 'vId' => $FlowId);
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);
                }
            }
        }
    }
    
    /**
     * method that returns all the emails that have been registered to retouch them when creating a flow pass
     */
    public function contactEmail(){
        $this->autoRender = false;
        return json_encode(
            $this->FlowPass->ContactCatalog->find(
                'list', array(
                    'conditions'=>array('ContactCatalog.type'=>1),
                    'fields' => array('id', 'value'),
                )
            )
        );
    }

    /**
     * method that returns all the template emails that have been registered to retouch them when creating a flow pass
     */
    public function templateEmail(){
        $this->autoRender = false;
        return json_encode(
            $this->FlowPass->TemplateInfromacion->find(
                'list', array(
                    'conditions'=>array('TemplateInfromacion.type'=>1),
                    'fields' => array('id', 'name'),
                )
            )
        );
    }

    /**
     * method that returns all the Numbers that have been registered to retouch them when creating a flow pass
     */
    public function contactWhatsApp(){
        $this->autoRender = false;
        return json_encode(
            $this->FlowPass->ContactCatalog->find(
                'list', array(
                    'conditions'=>array('ContactCatalog.type'=>2),
                    'fields' => array('id', 'value'),
                )
            )
        );
    }

    /**
     * method that returns all the template numbers that have been registered to retouch them when creating a flow pass
     */
    public function templateWhatsApp(){
        $this->autoRender = false;
        return json_encode(
            $this->FlowPass->TemplateInfromacion->find(
                'list', array(
                    'conditions'=>array('TemplateInfromacion.type'=>2),
                    'fields' => array('id', 'name'),
                )
            )
        );
    }

}
