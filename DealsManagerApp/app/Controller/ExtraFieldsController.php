<?php
App::uses('AppController', 'Controller');
/**
 * ExtraFields Controller
 *
 * @property ExtraField $ExtraField
 * @property PaginatorComponent $Paginator
 */
class ExtraFieldsController extends AppController {
/**
     * This controller uses following helpers
     *
     * @var array
     */
    var $helpers = array('Html', 'Form', 'Js', 'Paginator', 'Time');

    /**
     * This controller uses following components
     *
     * @var array
     */
    var $components = array('Auth', 'Cookie', 'Session', 'Paginator', 'RequestHandler', 'Flash');

/**
     * Called before the controller action.  You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        //check if login
        $this->checkLogin();
        //set layout
        $this->layout = 'admin';
        //check if admin or staff
        $this->checkAdminStaff();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//check permissions
        $this->checkStaffPermission('31');
        //get all products
        $extraFields = $this->ExtraField->getAllExtraFiels();
        //set product variable for view
        $this->set(compact('extraFields'));
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('32');
        //--------- Post request  -----------
		if ($this->request->is('post')) {
			$this->ExtraField->create();
			if ($this->ExtraField->save($this->request->data)) {
				//success message
                $this->Flash->success(__('Request has been completed.'), array('key' => 'success', 'params' => array('class' => 'alert alert-info')));
            } else {
                //failure message
                $this->Flash->success(__('Request has been not completed.'), array('key' => 'fail', 'params' => array('class' => 'alert alert-danger')));
			}
			return $this->redirect(
				array('controller' => 'ExtraFields', 'action' => 'index')
			);
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('33');
        //--------- Post request  -----------
        if ($this->request->is('post')) {
            //--------- Ajax request  -----------
            if ($this->RequestHandler->isAjax()) {
                $this->layout = 'ajax';
				//common variables
				$field = $this->request->data['field_name'];
                $this->request->data['ExtraField']['id'] = $this->request->data['pk'];  
				$this->request->data['ExtraField']['field_name'] = $this->request->data['value'];
                //save product
                $success = $this->ExtraField->save($this->request->data);
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success');
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);
                }
            }
        }
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('34');
        $ExtraFieldId = $this->request->data['ExtraField']['id'];

        //if product id exist
        if (!empty($ExtraFieldId)) {
            //--------- Post/Ajax request  -----------
            if ($this->request->isPost() || $this->RequestHandler->isAjax()) {
                //delete product
                $success = $this->ExtraField->delete($ExtraFieldId, false);
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success', 'vId' => $ExtraFieldId);
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);
                }
            }
        }
	}
}
