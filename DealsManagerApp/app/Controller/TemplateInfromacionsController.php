<?php
App::uses('AppController', 'Controller');
/**
 * TemplateInfromacions Controller
 *
 * @property TemplateInfromacion $TemplateInfromacion
 * @property PaginatorComponent $Paginator
 */
class TemplateInfromacionsController extends AppController {

/**
     * This controller uses following models
     *
     * @var array
     */
    public $uses = array('TemplateInfromacion');

/**
     * This controller uses following helpers
     *
     * @var array
     */
    var $helpers = array('Html', 'Form', 'Js', 'Paginator', 'Time');

    /**
     * This controller uses following components
     *
     * @var array
     */
    var $components = array('Auth', 'Cookie', 'Session', 'Paginator', 'RequestHandler', 'Flash');

	/**
     * Called before the controller action.  You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        //check if login
        $this->checkLogin();
        //set layout
        $this->layout = 'admin';
        //check if admin or staff
        $this->checkAdminStaff();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//check permissions
        $this->checkStaffPermission('31');
        $this->TemplateInfromacion->recursive = 0;
		$this->set('templateInfromacions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TemplateInfromacion->exists($id)) {
			throw new NotFoundException(__('Invalid template infromacion'));
		}
		$options = array('conditions' => array('TemplateInfromacion.' . $this->TemplateInfromacion->primaryKey => $id));
		$this->set('templateInfromacion', $this->TemplateInfromacion->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('32');
		if ($this->request->is('post')) {
			$this->TemplateInfromacion->create();
			if ($this->TemplateInfromacion->save($this->request->data)) {
				//success message
                $this->Flash->success(__('Request has been completed.'), array('key' => 'success', 'params' => array('class' => 'alert alert-info')));
            } else {
                //failure message
                $this->Flash->success(__('Request has been not completed.'), array('key' => 'fail', 'params' => array('class' => 'alert alert-danger')));
			}
			return $this->redirect(
				array('controller' => 'TemplateInfromacions', 'action' => 'index')
			);
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('33');
        //--------- Post request  -----------
        if ($this->request->is('post')) {
            //--------- Ajax request  -----------
            if ($this->RequestHandler->isAjax()) {
				$this->layout = 'ajax';
				//common variables
                $this->request->data['TemplateInfromacion']['id'] = $this->request->data['pk'];  
                $this->request->data['TemplateInfromacion']['name'] = $this->request->data['value'];
                //save product
                $success = $this->TemplateInfromacion->save($this->request->data);
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success');
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);
                }
            }
        }
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('34');
		$TemplateId = $this->request->data['TemplateInfromacion']['id'];
        //if product id exist
        if (!empty($TemplateId)) {
            //--------- Post/Ajax request  -----------
            if ($this->request->isPost() || $this->RequestHandler->isAjax()) {
                //delete product
                $success = $this->TemplateInfromacion->delete($TemplateId, false);
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success', 'vId' => $TemplateId);
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);
                }
            }
        }
    }
    
    public function updateContent(){
        // autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('33');
        //--------- Post request  -----------
        if ($this->request->is('post')) {
            //common variables
            $id=$this->request->data['TemplateInfromacion']['id'];
            //save product
            $success = $this->TemplateInfromacion->save($this->request->data);
            if ($success) {
                return $this->redirect(
                    array('controller' => 'TemplateInfromacions', 'action' => 'view',$id)
                );
            }
        }
    }

}
