<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * FlowPasses Controller
 *
 * @property FlowPass $FlowPass
 * @property PaginatorComponent $Paginator
 */
class FlowPassesController extends AppController {


    /**
     * This controller uses following models
     *
     * @var array
     */
    public $uses = array('Flow','FlowPass','ContactCatalog', 'Pipeline','Deal', 'Source', 'Contact', 'Product', 'User', 'AppFile', 'NoteDeal', 'Task', 'History', 'Pipeline', 'Stage', 'Timeline', 'LabelDeal', 'Discussion', 'UserDeal', 'SourceDeal', 'ProductDeal', 'ContactDeal', 'CustomDeal', 'Custom', 'Invoice', 'Label', 'Company', 'UserGroup');

	
	/**
     * This controller uses following helpers
     *
     * @var array
     */
    var $helpers = array('Html', 'Form', 'Js', 'Paginator', 'Time');

    /**
     * This controller uses following components
     *
     * @var array
     */
    var $components = array('Auth', 'Cookie', 'Session', 'Paginator', 'RequestHandler', 'Flash','Email');

	/**
     * Called before the controller action.  You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        //check if login
        $this->checkLogin();
        //set layout
        $this->layout = 'admin';
        //check if admin or staff
        $this->checkAdminStaff();
    }


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FlowPass->recursive = 0;
		$this->set('flowPasses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FlowPass->exists($id)) {
			throw new NotFoundException(__('Invalid flow pass '));
		}
		$options = array('conditions' => array('FlowPass.'.$this->FlowPass->primaryKey => $id));
		$this->set('flowPass', $this->FlowPass->find('first', $options));
	}

/**
 * add method
 * four validation blocks are found to allocate time lapse, 
 * and if no time lapse is chosen the insertion of a flow having a calf and a contact id is made
 * @return void
 */
	public function add() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('32');
		 if ($this->request->is('post')) {
            $data=null;
            if($this->request->data['FlowPass']['Timelapse']==1&&!empty($this->request->data['FlowPass']['timeHour'])){
                $data=array(
                    'FlowPass'=>
                        array(
                        'action_type'=>$this->request->data['FlowPass']['action_type'],
                        'template_infromacion_id'=>null,
                        'contact_catalog_id'=>null,
                        'time'=>$this->request->data['FlowPass']['timeHour']." h",
                        'flow_id'=>$this->request->data['FlowPass']['flow_id'],
                        'position'=>$this->request->data['FlowPass']['position'],)
                    );
            }
            if($this->request->data['FlowPass']['Timelapse']==2&&!empty($this->request->data['FlowPass']['timeDay'])){
                $data=array(
                    'FlowPass'=>
                        array(
                        'action_type'=>$this->request->data['FlowPass']['action_type'],
                        'template_infromacion_id'=>null,
                        'contact_catalog_id'=>null,
                        'time'=>$this->request->data['FlowPass']['timeDay']." d",
                        'flow_id'=>$this->request->data['FlowPass']['flow_id'],
                        'position'=>$this->request->data['FlowPass']['position'],)
                    );
            }
            if($this->request->data['FlowPass']['Timelapse']==3&&!empty($this->request->data['FlowPass']['timeWeek'])){
                $data=array(
                    'FlowPass'=>
                        array(
                        'action_type'=>$this->request->data['FlowPass']['action_type'],
                        'template_infromacion_id'=>null,
                        'contact_catalog_id'=>null,
                        'time'=>$this->request->data['FlowPass']['timeWeek']." w",
                        'flow_id'=>$this->request->data['FlowPass']['flow_id'],
                        'position'=>$this->request->data['FlowPass']['position'],)
                    );
            }
            if($this->request->data['FlowPass']['Timelapse']==4&&!empty($this->request->data['FlowPass']['timeMonth'])){
                $data=array(
                    'FlowPass'=>
                        array(
                        'action_type'=>$this->request->data['FlowPass']['action_type'],
                        'template_infromacion_id'=>null,
                        'contact_catalog_id'=>null,
                        'time'=>$this->request->data['FlowPass']['timeMonth']." m",
                        'flow_id'=>$this->request->data['FlowPass']['flow_id'],
                        'position'=>$this->request->data['FlowPass']['position'],)
                    );
            }
            if($this->request->data['FlowPass']['template']!=null||$this->request->data['FlowPass']['contact']!=null){
                $data=array(
                    'FlowPass'=>
                        array(
                        'action_type'=>$this->request->data['FlowPass']['action_type'],
                        'template_infromacion_id'=>$this->request->data['FlowPass']['template'],
                        'contact_catalog_id'=>$this->request->data['FlowPass']['contact'],
                        'time'=>"",
                        'flow_id'=>$this->request->data['FlowPass']['flow_id'],
                        'position'=>$this->request->data['FlowPass']['position'],)
                    );
            }
			$this->FlowPass->create();
			if ($this->FlowPass->save($data)) {
				//success message
                $this->Flash->success(__('Request has been completed.'), array('key' => 'success', 'params' => array('class' => 'alert alert-info')));
            } else {
                //failure message
                $this->Flash->success(__('Request has been not completed.'), array('key' => 'fail', 'params' => array('class' => 'alert alert-danger')));
			}
			return $this->redirect(
				array('controller' => 'Flows', 'action' => 'view',$this->request->data['FlowPass']['flow_id'])
			);
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FlowPass->exists($id)) {
			throw new NotFoundException(__('Invalid flow pass'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FlowPass->save($this->request->data)) {
				$this->Flash->success(__('The flow pass has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The flow pass could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FlowPass.' . $this->FlowPass->primaryKey => $id));
			$this->request->data = $this->FlowPass->find('first', $options);
		}
		$templateInfromacions = $this->FlowPass->TemplateInfromacion->find('list');
		$flows = $this->FlowPass->Flow->find('list');
		$contactCatalogs = $this->FlowPass->ContactCatalog->find('list');
		$this->set(compact('templateInfromacions', 'flows', 'contactCatalogs'));
	}


	/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('34');
		$FlowPassId = $this->request->data['Flowpass']['id'];
        //if product id exist
        if (!empty($FlowPassId)) {
            //--------- Post/Ajax request  -----------
            if ($this->request->isPost() || $this->RequestHandler->isAjax()) {
                //delete product
				$success = $this->FlowPass->delete($FlowPassId, false);
				//$success = true;
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success', 'vId' => $FlowPassId);
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);

                }
            }
        }
	}

	/**
     * This function is used to change the Flow Passes order in the Flow view.
     *
     * @return void
     */
    public function update()
    {
        // autorender off for view
        $this->autoRender = false;
        $flow = $this->request->data['item'];
        $count = 1;
        foreach ($flow as $row):
            $this->request->data['FlowPass']['id'] = $row;
            $this->request->data['FlowPass']['position'] = $count;
            //update stage order
            $result = $this->FlowPass->save($this->request->data);
            $count++;
        endforeach;
    }


    public function executionFlow(){
        $this->autoRender = false;
        if ($this->RequestHandler->isAjax()||$this->request->isGet()) {
            $this->layout = 'ajax';
            //common variables
            $stageId=$this->request->data['stageId'];
            $flows = $this->Flow->find('all',array('conditions' => array('Flow.stage_id'=> 13)));
            $flowpassfind = array('conditions' => array('FlowPass.flow_id'=> $flows[0]['Flow']['id']),'order' => 'FlowPass.position');
            $FlowPass=$this->FlowPass->find('all', $flowpassfind);
            if($FlowPass){
                foreach ($FlowPass as $type) {
                    $contact=null;
                    $content=null;
                    if ($type['FlowPass']['action_type']==1) {
                        $contact=$type['ContactCatalog']['value'];
                        $content=$type['TemplateInfromacion']['content'];
                        $this->sendEmail($contact,$content);
                    }else if ($type['FlowPass']['action_type']==2) {
                        $contact=$type['ContactCatalog']['value'];
                        $content=$type['TemplateInfromacion']['content'];
                        $this->sendMessage($contact,$content);
                    }else if ($type['FlowPass']['action_type']==3) {
                        $time=$type['FlowPass']['time'];
                        $this->timelapse($time);
                    }
                }
            }
        }
    }

    public function sendEmail($contact,$content){
        $this->autoRender = false;
        /* echo '\nEmail '.
             '\nContacto: '.$contact.
             '\nContenido: '.$content; */
             
        $Email = new CakeEmail();
        $Email->from(array($contact => 'My Site'));
        $Email->config('gmail');  
        $Email->to('danielb080499@gmail.com');
        $Email->subject('About');
        $Email->send($content);
        echo 'correo enviado';
    }

    public function sendMessage($contact,$content){
        $this->autoRender = false;
        /* echo '\nNumero '.
             '\nContacto: '.$contact.
             '\nContenido: '.$content; */
    }   

    public function timelapse($time){
        $this->autoRender = false;
        //echo '\ntimelapse: '.$time;
    }


}
