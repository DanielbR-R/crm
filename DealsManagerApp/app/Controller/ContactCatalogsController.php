<?php
App::uses('AppController', 'Controller');
/**
 * ContactCatalogs Controller
 *
 * @property ContactCatalog $ContactCatalog
 * @property PaginatorComponent $Paginator
 */
class ContactCatalogsController extends AppController {

/**
     * This controller uses following models
     *
     * @var array
     */
    public $uses = array('ContactCatalog');

/**
     * This controller uses following helpers
     *
     * @var array
     */
    var $helpers = array('Html', 'Form', 'Js', 'Paginator', 'Time');

    /**
     * This controller uses following components
     *
     * @var array
     */
    var $components = array('Auth', 'Cookie', 'Session', 'Paginator', 'RequestHandler', 'Flash');

	/**
     * Called before the controller action.  You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        //check if login
        $this->checkLogin();
        //set layout
        $this->layout = 'admin';
        //check if admin or staff
        $this->checkAdminStaff();
    }
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		//check permissions
        $this->checkStaffPermission('31');
		$this->ContactCatalog->recursive = 0;
		$this->set('contactCatalogs', $this->Paginator->paginate());
	}
/**
 * add method
 *
 * @return void
 */
	public function add() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('32');
		if ($this->request->is('post')) {
			$this->ContactCatalog->create();
			if ($this->ContactCatalog->save($this->request->data)) {
				//success message
                $this->Flash->success(__('Request has been completed.'), array('key' => 'success', 'params' => array('class' => 'alert alert-info')));
            } else {
                //failure message
                $this->Flash->success(__('Request has been not completed.'), array('key' => 'fail', 'params' => array('class' => 'alert alert-danger')));
			}
			return $this->redirect(
				array('controller' => 'ContactCatalogs', 'action' => 'index')
			);
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('33');
        //--------- Post request  -----------
        if ($this->request->is('post')) {
            //--------- Ajax request  -----------
            if ($this->RequestHandler->isAjax()) {
				$this->layout = 'ajax';
				//common variables
				$field = $this->request->data['value'];
                $this->request->data['ContactCatalog']['id'] = $this->request->data['pk'];  
				$this->request->data['ContactCatalog']['value'] = $this->request->data['value'];
                //save product
                $success = $this->ContactCatalog->save($this->request->data);
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success');
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);
                }
            }
        }
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		// autorender off for view
        $this->autoRender = false;
        //check permissions
        $this->checkStaffPermission('34');
		$ContactId = $this->request->data['ContactCatalog']['id'];
        //if product id exist
        if (!empty($ContactId)) {
            //--------- Post/Ajax request  -----------
            if ($this->request->isPost() || $this->RequestHandler->isAjax()) {
                //delete product
                $success = $this->ContactCatalog->delete($ContactId, false);
                if ($success) {
                    //return json success message
                    $response = array('bug' => 0, 'msg' => 'success', 'vId' => $ContactId);
                    return json_encode($response);
                } else {
                    //return json failure message
                    $response = array('bug' => 1, 'msg' => 'failure');
                    return json_encode($response);
                }
            }
        }
	}
}
