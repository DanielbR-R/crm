<div class="flows form">
<?php echo $this->Form->create('Flow'); ?>
	<fieldset>
		<legend><?php echo __('Add Flow'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('stage_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Flows'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stages'), array('controller' => 'stages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stage'), array('controller' => 'stages', 'action' => 'add')); ?> </li>
	</ul>
</div>
