-- Database: `dm`
-- Generation time: Wed 13th Nov 2019 21:07:27


DROP TABLE IF EXISTS announcements;

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission` int(1) NOT NULL,
  `read` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO announcements VALUES('1','anuncio','anuncio prueba','2019-11-13','2019-11-13','1','2','0');



DROP TABLE IF EXISTS backup;

CREATE TABLE `backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `file_size` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS company;

CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groups` mediumtext,
  `name` varchar(255) NOT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `address` mediumtext NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `description` mediumtext,
  `website` varchar(255) NOT NULL,
  `skype` mediumtext NOT NULL,
  `facebook` mediumtext NOT NULL,
  `linkedIn` mediumtext NOT NULL,
  `twitter` mediumtext NOT NULL,
  `youtube` mediumtext,
  `google_plus` mediumtext,
  `pinterest` mediumtext,
  `tumblr` mediumtext,
  `instagram` mediumtext,
  `github` mediumtext,
  `digg` mediumtext,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `id_3` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO company VALUES('1',NULL,'Company Demo','3452357809','company@example.com','Demo Address','California','USA','123456','USA','Demo company description is here','','','','','','','','','','','','','2018-10-30 00:56:46','2018-10-30 00:56:46');
INSERT INTO company VALUES('3','1','Uttec','+525552116931','uttec@uttec.com','San martin Azcatepec, Mexico','Mexico','MÃ©xico','55123','MÃ©xico','universidad','','','','','','','','','','','','','2019-11-13 18:59:28','2019-11-13 18:59:28');



DROP TABLE IF EXISTS contact;

CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` mediumtext NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip_code` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `description` mediumtext,
  `website` mediumtext,
  `company_id` int(11) DEFAULT NULL,
  `user_id` varchar(45) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedIn` varchar(100) NOT NULL,
  `skype` varchar(100) NOT NULL,
  `youtube` mediumtext,
  `google_plus` mediumtext,
  `pinterest` mediumtext,
  `tumblr` mediumtext,
  `instagram` mediumtext,
  `github` mediumtext,
  `digg` mediumtext,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO contact VALUES('1','contacto','contacto@prueba.com','5512345678','direccion','ciudad','estado','55123','paiz','trabajo','user.png','locacion',NULL,NULL,'1','1','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-13 19:01:15','2019-11-13 19:01:15');



DROP TABLE IF EXISTS contact_catalog;

CREATE TABLE `contact_catalog` (
  `id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created` varchar(255) NOT NULL,
  `updated` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS contact_deals;

CREATE TABLE `contact_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `deal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contact_deals_deal1_idx` (`deal_id`),
  KEY `fk_contact_deals_contact1_idx` (`contact_id`),
  CONSTRAINT `fk_contact_deals_contact1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contact_deals_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO contact_deals VALUES('1','1','1');



DROP TABLE IF EXISTS custom_company;

CREATE TABLE `custom_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_custom_company_custom_fields1_idx` (`custom_id`),
  KEY `fk_custom_company_company1_idx` (`company_id`),
  CONSTRAINT `fk_custom_company_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_custom_company_custom_fields1` FOREIGN KEY (`custom_id`) REFERENCES `custom_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO custom_company VALUES('2','3','','3');



DROP TABLE IF EXISTS custom_contacts;

CREATE TABLE `custom_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_custom_contacts_custom_fields1_idx` (`custom_id`),
  KEY `fk_custom_contacts_contact1_idx` (`contact_id`),
  CONSTRAINT `fk_custom_contacts_contact1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_custom_contacts_custom_fields1` FOREIGN KEY (`custom_id`) REFERENCES `custom_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO custom_contacts VALUES('1','2','','1');



DROP TABLE IF EXISTS custom_deals;

CREATE TABLE `custom_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `deal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_custom_deals_deal1_idx` (`deal_id`),
  KEY `fk_custom_deals_custom_fields1_idx` (`custom_id`),
  CONSTRAINT `fk_custom_deals_custom_fields1` FOREIGN KEY (`custom_id`) REFERENCES `custom_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_custom_deals_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO custom_deals VALUES('1','1','','1');



DROP TABLE IF EXISTS custom_fields;

CREATE TABLE `custom_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(2) NOT NULL,
  `module` int(2) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO custom_fields VALUES('1','Custom Field','1','1','2018-10-29 23:23:23');
INSERT INTO custom_fields VALUES('2','Custom Field','1','2','2018-10-29 23:23:38');
INSERT INTO custom_fields VALUES('3','Custom Field','1','3','2018-10-29 23:24:17');



DROP TABLE IF EXISTS deal;

CREATE TABLE `deal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `stage_id` int(11) NOT NULL,
  `pipeline_id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `permission` mediumtext,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_deal_stages1_idx` (`stage_id`),
  KEY `fk_deal_pipeline1_idx` (`pipeline_id`),
  CONSTRAINT `fk_deal_pipeline1` FOREIGN KEY (`pipeline_id`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_deal_stages1` FOREIGN KEY (`stage_id`) REFERENCES `stages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO deal VALUES('1','trato','1','0','1','17','3','2','3',NULL,'2019-11-13 19:06:08','2019-11-13 19:06:52');



DROP TABLE IF EXISTS discussion;

CREATE TABLE `discussion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` mediumtext NOT NULL,
  `deal_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `parent` int(2) NOT NULL DEFAULT '0',
  `type` int(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_discussion_users1_idx` (`users_id`),
  KEY `fk_discussion_deal1_idx` (`deal_id`),
  CONSTRAINT `fk_discussion_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_discussion_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS events;

CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `color` varchar(20) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO events VALUES('1','eventro prueba','ejemplo','2019-11-14','2019-11-15','31353C','1',NULL,'1');



DROP TABLE IF EXISTS expenses;

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(10,2) NOT NULL,
  `deal_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_expenses_expenses_category1_idx` (`category_id`),
  CONSTRAINT `fk_expenses_expenses_category1` FOREIGN KEY (`category_id`) REFERENCES `expenses_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS expenses_category;

CREATE TABLE `expenses_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO expenses_category VALUES('1','Expense Category','Description');



DROP TABLE IF EXISTS extra_field;

CREATE TABLE `extra_field` (
  `id` int(11) NOT NULL,
  `pipeline_id` int(11) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_campo_extra_pipeline1_idx` (`pipeline_id`),
  CONSTRAINT `fk_campo_extra_pipeline1` FOREIGN KEY (`pipeline_id`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS files;

CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `user_id` int(11) NOT NULL,
  `deal_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_files_users1_idx` (`user_id`),
  KEY `fk_files_deal1_idx` (`deal_id`),
  CONSTRAINT `fk_files_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_files_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS flow;

CREATE TABLE `flow` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flujo_stages1_idx` (`stage_id`),
  CONSTRAINT `fk_flujo_stages1` FOREIGN KEY (`stage_id`) REFERENCES `stages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS flow_pass;

CREATE TABLE `flow_pass` (
  `id` int(11) NOT NULL,
  `template_infromacion_id` int(11) NOT NULL,
  `flow_id` int(11) NOT NULL,
  `contact_catalog_id` int(11) NOT NULL,
  `action_type` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flow_pass_template_infromacion1_idx` (`template_infromacion_id`),
  KEY `fk_flow_pass_flow1_idx` (`flow_id`),
  KEY `fk_flow_pass_contact_catalog1_idx` (`contact_catalog_id`),
  CONSTRAINT `fk_flow_pass_contact_catalog1` FOREIGN KEY (`contact_catalog_id`) REFERENCES `contact_catalog` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_flow_pass_flow1` FOREIGN KEY (`flow_id`) REFERENCES `flow` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_flow_pass_template_infromacion1` FOREIGN KEY (`template_infromacion_id`) REFERENCES `template_infromacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS history;

CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deal_id` int(11) NOT NULL,
  `deal_name` varchar(255) NOT NULL,
  `reason` mediumtext,
  `pipeline` varchar(250) NOT NULL,
  `stage` varchar(250) NOT NULL,
  `user` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `status` smallint(2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_history_users1_idx` (`user_id`),
  KEY `fk_history_deal1_idx` (`deal_id`),
  KEY `fk_history_user_groups1_idx` (`group_id`),
  CONSTRAINT `fk_history_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_history_user_groups1` FOREIGN KEY (`group_id`) REFERENCES `user_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_history_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS invoice_products;

CREATE TABLE `invoice_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` mediumtext NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_unit_price` decimal(10,2) NOT NULL,
  `product_total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_invoice_products_invoices1_idx` (`invoice_id`),
  CONSTRAINT `fk_invoice_products_invoices1` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS invoices;

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `deal_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `issue_date` date NOT NULL,
  `due_date` date NOT NULL,
  `currency` varchar(250) DEFAULT NULL,
  `note` mediumtext,
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `custom_tax` decimal(10,2) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_invoices_deal1_idx` (`deal_id`),
  KEY `fk_invoices_custom_fields1_idx` (`custom_id`),
  CONSTRAINT `fk_invoices_custom_fields1` FOREIGN KEY (`custom_id`) REFERENCES `custom_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoices_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS label_deals;

CREATE TABLE `label_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label_id` int(11) NOT NULL,
  `deal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_label_deals_deal1_idx` (`deal_id`),
  KEY `fk_label_deals_labels1_idx` (`label_id`),
  CONSTRAINT `fk_label_deals_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_label_deals_labels1` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS labels;

CREATE TABLE `labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `pipeline_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_labels_pipeline1_idx` (`pipeline_id`),
  CONSTRAINT `fk_labels_pipeline1` FOREIGN KEY (`pipeline_id`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO labels VALUES('1','Service','label-one','1');
INSERT INTO labels VALUES('2','Social','label-two','1');
INSERT INTO labels VALUES('3','Digital','label-three','1');
INSERT INTO labels VALUES('4','label prueba','label-six','3');



DROP TABLE IF EXISTS message;

CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` mediumtext NOT NULL,
  `message_to` int(11) NOT NULL,
  `message_by` int(11) NOT NULL,
  `read` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO message VALUES('1','hola cliente prueba','2','1','0','2019-11-13 19:02:49');



DROP TABLE IF EXISTS note_deals;

CREATE TABLE `note_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` text CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deal_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_note_deals_deal1_idx` (`deal_id`),
  CONSTRAINT `fk_note_deals_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO note_deals VALUES('1','algo','1','1','2019-11-13 19:06:08','2019-11-13 19:06:08');



DROP TABLE IF EXISTS payment_methods;

CREATE TABLE `payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS payments;

CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `deal_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_date` date NOT NULL,
  `method` int(11) NOT NULL,
  `note` mediumtext,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_payments_users1_idx` (`user_id`),
  KEY `fk_payments_deal1_idx` (`deal_id`),
  KEY `fk_payments_invoices1_idx` (`invoice_id`),
  CONSTRAINT `fk_payments_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_payments_invoices1` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_payments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS pipeline;

CREATE TABLE `pipeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO pipeline VALUES('1','Pipeline Default','2018-10-25 22:10:44');
INSERT INTO pipeline VALUES('3','Flujo','2019-11-13 19:01:47');



DROP TABLE IF EXISTS pipeline_permission;

CREATE TABLE `pipeline_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pipeline_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pipeline_permission_pipeline1_idx` (`pipeline_id`),
  KEY `fk_pipeline_permission_users1_idx` (`user_id`),
  CONSTRAINT `fk_pipeline_permission_pipeline1` FOREIGN KEY (`pipeline_id`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pipeline_permission_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS product_deals;

CREATE TABLE `product_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `deal_id` int(11) NOT NULL,
  `extra_field_id` int(11) NOT NULL,
  `value` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_product_deals_deal1_idx` (`deal_id`),
  KEY `fk_product_deals_products1_idx` (`product_id`),
  KEY `fk_product_deals_extra_field1_idx` (`extra_field_id`),
  CONSTRAINT `fk_product_deals_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_deals_extra_field1` FOREIGN KEY (`extra_field_id`) REFERENCES `extra_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_deals_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS products;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pipeline_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_products_pipeline1_idx` (`pipeline_id`),
  CONSTRAINT `fk_products_pipeline1` FOREIGN KEY (`pipeline_id`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS settings;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` int(2) NOT NULL,
  `title_text` varchar(255) NOT NULL,
  `title_logo` varchar(255) NOT NULL,
  `language` varchar(50) NOT NULL,
  `currency` varchar(20) NOT NULL,
  `currency_symbol` varchar(10) NOT NULL,
  `date_format` varchar(100) NOT NULL,
  `time_format` varchar(10) NOT NULL,
  `time_zone` varchar(50) NOT NULL,
  `pipeline` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_settings_pipeline1_idx` (`pipeline`),
  CONSTRAINT `fk_settings_pipeline1` FOREIGN KEY (`pipeline`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO settings VALUES('1','2','Deals Manager 2','dm.png','en','Dollars','$','Y-m-d','g:i A','America/Adak','1');



DROP TABLE IF EXISTS settings_company;

CREATE TABLE `settings_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` mediumtext NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `country` varchar(100) NOT NULL,
  `telephone` int(11) NOT NULL,
  `system_email` varchar(100) NOT NULL,
  `system_email_from` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO settings_company VALUES('1','Deals Manager 2','New York ,USA','New York','New York','160000','USA','12345678','dealsmanager@gmail.com','dealsmanager@gmail.com');



DROP TABLE IF EXISTS settings_email;

CREATE TABLE `settings_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protocol` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `port` int(10) DEFAULT NULL,
  `message` int(2) NOT NULL DEFAULT '0',
  `ticket` int(2) NOT NULL DEFAULT '0',
  `add_user` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO settings_email VALUES('1','0','','','',NULL,'0','0','0');



DROP TABLE IF EXISTS source;

CREATE TABLE `source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO source VALUES('1','source','2019-11-13 19:02:27');



DROP TABLE IF EXISTS source_deals;

CREATE TABLE `source_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) DEFAULT NULL,
  `deal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_source_deals_deal1_idx` (`deal_id`),
  CONSTRAINT `fk_source_deals_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO source_deals VALUES('1','1','1');



DROP TABLE IF EXISTS stages;

CREATE TABLE `stages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` int(2) NOT NULL,
  `pipeline_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stages_pipeline1_idx` (`pipeline_id`),
  CONSTRAINT `fk_stages_pipeline1` FOREIGN KEY (`pipeline_id`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

INSERT INTO stages VALUES('1','Idea','1','1','2018-10-25 22:10:44');
INSERT INTO stages VALUES('2','Qualified','2','1','2018-10-25 22:10:44');
INSERT INTO stages VALUES('3','Proposal','3','1','2018-10-25 22:10:44');
INSERT INTO stages VALUES('4','Negotation','4','1','2018-10-25 22:10:44');
INSERT INTO stages VALUES('5','Final','5','1','2018-10-25 22:10:44');
INSERT INTO stages VALUES('12','Idea','2','3','2019-11-13 19:01:47');
INSERT INTO stages VALUES('13','Qualified','3','3','2019-11-13 19:01:47');
INSERT INTO stages VALUES('14','Proposal','4','3','2019-11-13 19:01:47');
INSERT INTO stages VALUES('15','Negotation','5','3','2019-11-13 19:01:47');
INSERT INTO stages VALUES('16','Final','6','3','2019-11-13 19:01:47');
INSERT INTO stages VALUES('17','Fase','1','3','2019-11-13 19:01:54');



DROP TABLE IF EXISTS tasks;

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` mediumtext NOT NULL,
  `motive` int(1) NOT NULL,
  `priority` int(1) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `note` mediumtext,
  `deal_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO tasks VALUES('1','tarea ejemplo','1','1','2019-11-13','01:15:00','0','correo','1','1','2019-11-13 19:07:15');



DROP TABLE IF EXISTS taxes;

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO taxes VALUES('1','Vat','10.00');



DROP TABLE IF EXISTS template_infromacion;

CREATE TABLE `template_infromacion` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pipeline_id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `type` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_plantilla_infromacion_pipeline1_idx` (`pipeline_id`),
  CONSTRAINT `fk_plantilla_infromacion_pipeline1` FOREIGN KEY (`pipeline_id`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS ticket_message;

CREATE TABLE `ticket_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` mediumtext NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ticket_message_users1_idx` (`user_id`),
  CONSTRAINT `fk_ticket_message_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS ticket_type;

CREATE TABLE `ticket_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO ticket_type VALUES('1','General','General description');
INSERT INTO ticket_type VALUES('2','Service','Services Description');



DROP TABLE IF EXISTS tickets;

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL,
  `message` mediumtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `assign` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `attachment` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tickets_ticket_type1_idx` (`type_id`),
  CONSTRAINT `fk_tickets_ticket_type1` FOREIGN KEY (`type_id`) REFERENCES `ticket_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO tickets VALUES('1','0','sujeto','<p>tiket</p>','0','1','3','1','3',NULL,'302187605servidor.txt','2019-11-13 19:03:17');



DROP TABLE IF EXISTS timeline;

CREATE TABLE `timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(250) NOT NULL,
  `module` varchar(50) NOT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `pipeline_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_timeline_pipeline1_idx` (`pipeline_id`),
  CONSTRAINT `fk_timeline_pipeline1` FOREIGN KEY (`pipeline_id`) REFERENCES `pipeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO timeline VALUES('1','trato','add_Deal','1','3','1','Matthew Smith','2019-11-13 19:06:08');
INSERT INTO timeline VALUES('2','source','add_Source','1','3','1','Matthew Smith','2019-11-13 19:06:08');
INSERT INTO timeline VALUES('3','contacto','add_Contact','1','3','1','Matthew Smith','2019-11-13 19:06:08');
INSERT INTO timeline VALUES('4','Fase to Idea','move_Stage','1','3','1','Matthew Smith','2019-11-13 19:06:39');
INSERT INTO timeline VALUES('5','Idea to Fase','move_Stage','1','3','1','Matthew Smith','2019-11-13 19:06:40');
INSERT INTO timeline VALUES('6','Fase to Idea','move_Stage','1','3','1','Matthew Smith','2019-11-13 19:06:47');
INSERT INTO timeline VALUES('7','Idea to Fase','move_Stage','1','3','1','Matthew Smith','2019-11-13 19:06:52');
INSERT INTO timeline VALUES('8','tarea ejemplo','add_Task','1','3','1','Matthew Smith','2019-11-13 19:07:15');



DROP TABLE IF EXISTS user_deals;

CREATE TABLE `user_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) DEFAULT NULL,
  `deal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_deals_deal1_idx` (`deal_id`),
  CONSTRAINT `fk_user_deals_deal1` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO user_deals VALUES('1','1','1');



DROP TABLE IF EXISTS user_details;

CREATE TABLE `user_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `location` varchar(256) DEFAULT NULL,
  `cellphone` varchar(15) DEFAULT NULL,
  `web_page` mediumtext,
  `about` mediumtext,
  `skype` mediumtext,
  `facebook` mediumtext,
  `twitter` mediumtext,
  `google_plus` mediumtext,
  `linkedin` mediumtext,
  `youtube` mediumtext,
  `pinterest` mediumtext,
  `digg` mediumtext,
  `github` mediumtext,
  `instagram` mediumtext,
  `tumblr` mediumtext,
  PRIMARY KEY (`id`),
  KEY `fk_user_details_users1_idx1` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO user_details VALUES('1','1','1','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO user_details VALUES('2','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO user_details VALUES('3','3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO user_details VALUES('4','4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO user_details VALUES('5','5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO user_details VALUES('6','6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);



DROP TABLE IF EXISTS user_groups;

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO user_groups VALUES('1','Marketing Team');
INSERT INTO user_groups VALUES('2','grupo prueba');



DROP TABLE IF EXISTS user_roles;

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` smallint(1) NOT NULL,
  `permission` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO user_roles VALUES('1','Admin Role','1','1,2,3,4,5,6,7,8,51');
INSERT INTO user_roles VALUES('2','Staff Role','2','11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44,51,52,53,54');
INSERT INTO user_roles VALUES('3','rol admin prueba','1','1,3,4,5,6,7,8');



DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `job_title` varchar(250) NOT NULL,
  `group_id` varchar(45) NOT NULL DEFAULT '0',
  `company_id` varchar(45) DEFAULT NULL,
  `picture` varchar(255) NOT NULL,
  `active` varchar(3) DEFAULT '0',
  `role` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`username`),
  KEY `mail` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO users VALUES('1','1','admin@admin.com','12a7b21289d320241dd1f2fcaf536d1a60ca63b2','admin@admin.com','Matthew','Smith','Super Admin','0','1','user.png','1','1','2014-08-20 05:01:54','2018-11-01 01:30:25');
INSERT INTO users VALUES('2','4','cliente@prueba.com','fa4fea8e60dd0dd2fb486247159e06a6f23a8533','cliente@prueba.com','cliente','prueba','Client','0','1','user.png','1',NULL,'2019-11-13 19:00:03','2019-11-13 19:00:03');
INSERT INTO users VALUES('3','4','cliente@uttec.com','fa4fea8e60dd0dd2fb486247159e06a6f23a8533','cliente@uttec.com','cliente','uttec','Client','0','3','user.png','1',NULL,'2019-11-13 19:00:22','2019-11-13 19:00:22');
INSERT INTO users VALUES('4','1','administrador@prueba.com','fa4fea8e60dd0dd2fb486247159e06a6f23a8533','administrador@prueba.com','administrador','prueba','prueba','0',NULL,'user.png','1','1','2019-11-13 19:04:04','2019-11-13 19:04:04');
INSERT INTO users VALUES('5','2','gerente@prueba.com','fa4fea8e60dd0dd2fb486247159e06a6f23a8533','gerente@prueba.com','gerente','prueba','prueba','1',NULL,'user.png','1','2','2019-11-13 19:04:29','2019-11-13 19:04:29');
INSERT INTO users VALUES('6','3','personal@prueba.com','fa4fea8e60dd0dd2fb486247159e06a6f23a8533','personal@prueba.com','personal','prueba','prueba','1',NULL,'user.png','1','2','2019-11-13 19:04:58','2019-11-13 19:04:58');



